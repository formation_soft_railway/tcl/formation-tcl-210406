# threads
package require Thread

tsv::set lapins resultat 0
tsv::set lapins2 troplong 0

# puts "Au bout de 6 mois : [nblapins 6]"
set threadcalcul [thread::create -joinable {
	proc nblapins {mois} {
		if {$mois<3 } {
			return $mois
		} elseif { [tsv::get lapins2 troplong] } {
			return Inf
		} else {
			return [expr 3*([nblapins [expr $mois-1]]+ \
				[nblapins [expr $mois-2]])/2 ]
		}
	}
		
	tsv::lock lapins {
		tsv::set lapins resultat [nblapins 36]
	}
}]
puts "Calcul"
puts "Au bout de 3 ans :"
for {set i 0} {[thread::exists $threadcalcul]} {incr i} {
	puts -nonewline "."
	flush stdout	
	after 100
	if {$i==100} { tsv::set lapins2 troplong 1 }
}
tsv::lock lapins {
	puts "[tsv::get lapins resultat]"
}
# plus necessaire, le thread est deja fini : thread::join $threadcalcul
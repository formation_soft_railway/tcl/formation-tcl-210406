# procedures du zoo
proc ditbonjour {} { puts "Hello Zoo !" }

proc ditbonjouralanimal {{nom ""} {excl !}} { 
	set text "Hello $nom $excl" 
	puts $text
}

# interdit : $text inconnu ici puts $text

proc ditbonjouranimaux args {
	foreach animal $args {
		ditbonjouralanimal $animal
	}
}

proc getcri {animal} {
	proc getcrifauve {} { return rugit }
	switch $animal {
		tigre { return [getcrifauve] }
		girafe { return meugle }
		faucon { return huit }
		default {return cri }
	}
}

# proc incr {nomvariable} { set $nomvariable [expr $$nomvariable+1] }
# set i 18
# incr i

proc tarifpour {visiteurs} {
	# global tarif
	# return [expr $tarif*$visiteurs]
	upvar tarif tarifloc
	return [expr $tarifloc*$visiteurs]
}

proc decr {nomvariable} {
	upvar $nomvariable nomvariableloc
	set nomvariableloc [expr $nomvariableloc-1]
}

proc afficheinfos {} {
	set tarif 10.3
	decr tarif
	puts "WE special pour 4 : [tarifpour 4]"
}

ditbonjour
ditbonjouralanimal
ditbonjouralanimal "Ann la girafe"
ditbonjouralanimal "Tom le tigre" !!!
ditbonjouranimaux Ann Tom Carol
puts "La girafe [getcri girafe]"
puts "Le tigre [getcri tigre]"
# inutile : global tarif
set tarif 8.4
puts "Tarif pour 2 : [tarifpour 2]"
afficheinfos
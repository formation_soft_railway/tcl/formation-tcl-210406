# declaration de package
# génération : pkg_mkIndex . package-voliere.tcl
# puis distribuer un zip avec package-voliere.tcl + ce fichier
package require Tcl 8.5
# package provide Voliere 1.2a1
# package provide Voliere 1.2a2
# package provide Voliere 1.2a3
# package provide Voliere 1.2b1
# package provide Voliere 1.2
package provide Voliere 1.2.1

namespace eval Voliere {
	variable especes [list Aigles Vautours Milans Peroquets]
	namespace export afficher 
	namespace export ajouter
}

proc Voliere::afficher {} {
	variable especes
	puts "Notre voliere accueille :"
	foreach espece $especes {
		puts "- des [string tolower $espece]"
	}
}

proc Voliere::ajouter {espece} {
	variable especes
	lappend especes $espece
}


# Voliere::afficher
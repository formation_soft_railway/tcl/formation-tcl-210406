# Classes et objets

oo::class create Animal {
	variable nom
	variable age
	variable espece
 	# constructor {} { }
	constructor {{n ""} {a 0} {e ""}} { 
		set nom $n
		set age $a
		set espece $e
	}
	destructor { puts "$nom detruit"}
	method setNom {n} { set nom $n }
	method setAge {a} { set age $a }
	method setAgeEnMois {m} { my setAge [expr $m/12] }
	method setEspece {e} { set espece $e }
	method afficher {} { puts "$nom ($espece) a $age ans"}
}

oo::class create Encadreur {
	method encadrer {} {
		puts "*********************"
		my afficher
		puts "*********************"
	}
}

oo::class create Reptile {
	superclass Animal
	mixin Encadreur
	variable amphibien
	constructor {{n ""} {a 0} {e ""} {am ""}} { 
		next $n $a $e
		set amphibien $am
	}
	method setAmphibien {am} { set amphibien $am }
	method afficher {} { 
		nextto Animal 
		if {$amphibien} {
			puts "- C'est un amphibien"
		} else {
			puts "- C'est un terrien"
		}
	}
}

set ann [Animal new]
$ann setNom "Ann"
$ann setAge 23
$ann setEspece Girafe
$ann afficher
#puts $ann

set tom [Animal new "Tom" 8 Tigre]
$tom afficher
$tom destroy

set franck [Reptile new "Phil" 3 Caiman 1]
# $franck afficher
$franck encadrer